import { MetaUpgradesSettings } from "../../Data/GameSettings";
import { MetaUpgradesData } from "../../Data/UserData";
import { MetaUpgradeType } from "../../Upgrades/UpgradeType";

export class MetaUpgrades {
    private upgradeTypeToValue = new Map<MetaUpgradeType, number>();
    public constructor(data: MetaUpgradesData, settings: MetaUpgradesSettings) {
        // 生命值
        this.upgradeTypeToValue.set(MetaUpgradeType.Health, this.getBonusValue(data.healthLevel, settings.health.bonuses));
        // 攻击伤害
        this.upgradeTypeToValue.set(MetaUpgradeType.OverallDamage, this.getBonusValue(data.overallDamageLevel, settings.overallDamage.bonuses));
        // 攻击穿透数量
        this.upgradeTypeToValue.set(
            MetaUpgradeType.ProjectilePiercing,
            this.getBonusValue(data.projectilePiercingLevel, settings.projectilePiercing.bonuses)
        );
        // 移动速度
        this.upgradeTypeToValue.set(MetaUpgradeType.MovementSpeed, this.getBonusValue(data.movementSpeedLevel, settings.movementSpeed.bonuses));
        // 经验采集
        this.upgradeTypeToValue.set(MetaUpgradeType.XPGatherer, this.getBonusValue(data.xpGathererLevel, settings.xpGatherer.bonuses));
        // 金币采集
        this.upgradeTypeToValue.set(MetaUpgradeType.GoldGatherer, this.getBonusValue(data.goldGathererLevel, settings.goldGatherer.bonuses));
    }

    /* 
     * 获取奖励，根据等级，获取对应的奖励
     * @param level 等级
     * @param bonuses 奖励
     */
    private getBonusValue(level: number, bonuses: number[]): number {
        if (level <= 0) return 0;
        if (bonuses.length < level) throw new Error(`Meta upgrade does not have settings for level ${level}`);

        return bonuses[level - 1];
    }

    /* 
     * 根据对应的类型，获取数据
     * @param type 类型
     */
    public getUpgradeValue(type: MetaUpgradeType): number {
        if (!this.upgradeTypeToValue.has(type)) {
            throw new Error("Does not have meta upgrade set up " + type);
        }

        return this.upgradeTypeToValue.get(type);
    }
}
