import { Canvas, Component, KeyCode, Vec2, _decorator, Node, approx } from "cc";
import { AppRoot } from "../AppRoot/AppRoot";
import { requireAppRootAsync } from "../AppRoot/AppRootUtils";
import { delay } from "../Services/Utils/AsyncUtils";
import { GameAudioAdapter } from "./Audio/GameAudioAdapter";
import { Background } from "./Background/Background";
import { MagnetCollisionSystem } from "./Collision/MagnetCollisionSystem";
import { PlayerCollisionSystem } from "./Collision/PlayerCollisionSystem";
import { PlayerProjectileCollisionSystem } from "./Collision/PlayerProjectileCollisionSystem";
import { WeaponCollisionSystem } from "./Collision/WeaponCollisionSystem";
import { GameSettings, PlayerSettings } from "./Data/GameSettings";
import { TranslationData } from "./Data/TranslationData";
import { UserData } from "./Data/UserData";
import { KeyboardInput } from "./Input/KeyboardInput";
import { MultiInput } from "./Input/MultiInput";
import { VirtualJoystic } from "./Input/VirtualJoystic";
import { ItemAttractor } from "./Items/ItemAttractor";
import { ItemManager } from "./Items/ItemManager";
import { GameModalLauncher } from "./ModalWIndows/GameModalLauncher";
import { Pauser } from "./Pauser";
import { TestValues } from "./TestGameRunner";
import { GameUI } from "./UI/GameUI";
import { EnemyDeathEffectSpawner } from "./Unit/Enemy/EnemyDeathEffectSpawner/EnemyDeathEffectSpawner";
import { EnemyManager } from "./Unit/Enemy/EnemyManager";
import { EnemyProjectileLauncher } from "./Unit/Enemy/ProjectileLauncher.cs/EnemyProjectileLauncher";
import { MetaUpgrades } from "./Unit/MetaUpgrades/MetaUpgrades";
import { Player, PlayerData } from "./Unit/Player/Player";
import { HaloProjectileLauncher } from "./Projectile/ProjectileLauncher/HaloProjectileLauncher";
import { ProjectileData } from "./Projectile/ProjectileLauncher/ProjectileData";
import { ProjectileLauncher } from "./Projectile/ProjectileLauncher/ProjectileLauncher";
import { WaveProjectileLauncher } from "./Projectile/ProjectileLauncher/WaveProjectileLauncher";
import { Upgrader } from "./Upgrades/Upgrader";
import { MetaUpgradeType } from "./Upgrades/UpgradeType";

const { ccclass, property } = _decorator;

@ccclass("Game")
export class Game extends Component {
    private static instance: Game;

    // 虚拟摇杆
    @property(VirtualJoystic) private virtualJoystic: VirtualJoystic;
    // 玩家组件
    @property(Player) private player: Player;
    // 武器组件
    @property(ProjectileLauncher) private haloProjectileLauncherComponent: ProjectileLauncher;
    @property(ProjectileLauncher) private horizontalProjectileLauncherComponent: ProjectileLauncher;
    @property(ProjectileLauncher) private diagonalProjectileLauncherComponent: ProjectileLauncher;
    @property(ProjectileLauncher) private enemyAxeProjectileLauncherComponent: ProjectileLauncher;
    @property(ProjectileLauncher) private enemyMagicOrbProjectileLauncherComponent: ProjectileLauncher;
    // 敌人组件
    @property(EnemyManager) private enemyManager: EnemyManager;
    // 死亡效果
    @property(EnemyDeathEffectSpawner) private deathEffectSpawner: EnemyDeathEffectSpawner;
    // 其他管理器
    @property(ItemManager) private itemManager: ItemManager;
    // 游戏UI
    @property(GameUI) private gameUI: GameUI;
    // 游戏画布
    @property(Canvas) private gameCanvas: Canvas;
    // 游戏背景
    @property(Background) private background: Background;
    // 游戏音效
    @property(GameAudioAdapter) private gameAudioAdapter: GameAudioAdapter;
    // 黑屏
    @property(Node) private blackScreen: Node;

    // 玩家碰撞系统
    private playerCollisionSystem: PlayerCollisionSystem;
    // 玩家武器
    private haloProjectileLauncher: HaloProjectileLauncher;
    private horizontalProjectileLauncher: WaveProjectileLauncher;
    private diagonalProjectileLauncher: WaveProjectileLauncher;

    // 敌人武器
    private enemyAxeProjectileLauncher: EnemyProjectileLauncher;
    private enemyMagicOrbProjectileLauncher: EnemyProjectileLauncher;

    // 经验吸取器
    private itemAttractor: ItemAttractor;

    // 暂停游戏
    private gamePauser: Pauser = new Pauser();
    // 游戏结果
    private gameResult: GameResult;

    // 游戏时间
    private timeAlive = 0;

    public static get Instance(): Game {
        return this.instance;
    }

    public start(): void {
        // 暂停游戏并激活黑屏
        this.gamePauser.pause();
        Game.instance = this;
        this.blackScreen.active = true;
    }

    /* 
     * 游戏开始
     * @param userData 用户数据
     * @param settings 游戏设置
     * @param translationData 语言设置
     * @param testValues 测试数据
     */
    public async play(userData: UserData, settings: GameSettings, translationData: TranslationData, testValues?: TestValues): Promise<GameResult> {
        await this.setup(userData, settings, translationData, testValues);

        // AppRoot.Instance.Analytics.gameStart();

        this.gamePauser.resume();
        this.blackScreen.active = false;
        AppRoot.Instance.ScreenFader.playClose();

        while (!this.gameResult.hasExitManually && this.player.Health.IsAlive) await delay(100);

        this.gamePauser.pause();
        Game.instance = null;
        this.gameResult.score = this.timeAlive;

        if (!this.gameResult.hasExitManually) {
            AppRoot.Instance.Analytics.goldPerRun(this.gameResult.goldCoins);
            AppRoot.Instance.Analytics.gameEnd(this.gameResult.score);

            await delay(2000);
        } else {
            AppRoot.Instance.Analytics.gameExit(this.timeAlive);
        }

        return this.gameResult;
    }

    // 退出游戏
    public exitGame(): void {
        this.gameResult.hasExitManually = true;
    }

    public update(deltaTime: number): void {
        if (this.gamePauser.IsPaused) return;

        this.player.gameTick(deltaTime);
        // 检测碰撞
        this.playerCollisionSystem.gameTick(deltaTime);
        this.enemyManager.gameTick(deltaTime);
        this.haloProjectileLauncher.gameTick(deltaTime);
        this.horizontalProjectileLauncher.gameTick(deltaTime);
        this.diagonalProjectileLauncher.gameTick(deltaTime);
        this.enemyAxeProjectileLauncher.gameTick(deltaTime);
        this.enemyMagicOrbProjectileLauncher.gameTick(deltaTime);
        this.itemAttractor.gameTick(deltaTime);
        this.background.gameTick();

        this.timeAlive += deltaTime;
        this.gameUI.updateTimeAlive(this.timeAlive);

        AppRoot.Instance.MainCamera.node.setWorldPosition(this.player.node.worldPosition);
        this.gameUI.node.setWorldPosition(this.player.node.worldPosition);
    }

    /* 
     * 初始化游戏
     * @param userData 用户数据
     * @param settings 游戏设置
     * @param translationData 语言设置
     * @param testValues 测试数据
    */
    private async setup(userData: UserData, settings: GameSettings, translationData: TranslationData, testValues: TestValues): Promise<void> {
        // 等待应用根节点加载完毕
        await requireAppRootAsync();
        // 主摄像机
        this.gameCanvas.cameraComponent = AppRoot.Instance.MainCamera;
        // 游戏结果
        this.gameResult = new GameResult();
        const metaUpgrades = new MetaUpgrades(userData.game.metaUpgrades, settings.metaUpgrades);

        this.virtualJoystic.init();

        const wasd = new KeyboardInput(KeyCode.KEY_W, KeyCode.KEY_S, KeyCode.KEY_A, KeyCode.KEY_D);
        const arrowKeys = new KeyboardInput(KeyCode.ARROW_UP, KeyCode.ARROW_DOWN, KeyCode.ARROW_LEFT, KeyCode.ARROW_RIGHT);
        const multiInput: MultiInput = new MultiInput([this.virtualJoystic, wasd, arrowKeys]);
        // 玩家初始化
        this.player.init(multiInput, this.createPlayerData(settings.player, metaUpgrades));
        // 敌人初始化
        this.enemyManager.init(this.player.node, settings.enemyManager);
        // 敌人死亡特效初始化
        this.deathEffectSpawner.init(this.enemyManager);

        // 玩家碰撞系统
        this.playerCollisionSystem = new PlayerCollisionSystem(this.player, settings.player.collisionDelay, this.itemManager);
        // 武器碰撞系统
        new WeaponCollisionSystem(this.player.Weapon);

        const projectileData = new ProjectileData();
        projectileData.damage = 1 + metaUpgrades.getUpgradeValue(MetaUpgradeType.OverallDamage);
        projectileData.pierces = 1 + metaUpgrades.getUpgradeValue(MetaUpgradeType.ProjectilePiercing);
        // 光环
        this.haloProjectileLauncher = new HaloProjectileLauncher(
            this.haloProjectileLauncherComponent,
            this.player.node,
            settings.player.haloLauncher,
            projectileData
        );
        // 水平子弹
        this.horizontalProjectileLauncher = new WaveProjectileLauncher(
            this.horizontalProjectileLauncherComponent,
            this.player.node,
            [new Vec2(0, 1), new Vec2(-0.1, 0.8), new Vec2(0.1, 0.8)],
            settings.player.horizontalLauncher,
            projectileData
        );
        // 对角子弹
        this.diagonalProjectileLauncher = new WaveProjectileLauncher(
            this.diagonalProjectileLauncherComponent,
            this.player.node,
            [new Vec2(-0.5, -0.5), new Vec2(0.5, -0.5)],
            settings.player.diagonalLauncher,
            projectileData
        );
        // 敌人斧子
        this.enemyAxeProjectileLauncher = new EnemyProjectileLauncher(
            this.enemyAxeProjectileLauncherComponent,
            this.player.node,
            this.enemyManager,
            settings.enemyManager.axeLauncher
        );
        // 敌人魔法球
        this.enemyMagicOrbProjectileLauncher = new EnemyProjectileLauncher(
            this.enemyMagicOrbProjectileLauncherComponent,
            this.player.node,
            this.enemyManager,
            settings.enemyManager.magicOrbLauncher
        );

        // 玩家子弹碰撞系统
        new PlayerProjectileCollisionSystem([this.haloProjectileLauncher, this.horizontalProjectileLauncher, this.diagonalProjectileLauncher]);
        // 物品吸引光环，可以自动吸取经验和金币，范围是100
        this.itemAttractor = new ItemAttractor(this.player.node, 100);
        // 磁铁碰撞系统
        new MagnetCollisionSystem(this.player.Magnet, this.itemAttractor);
        // 强化系统
        const upgrader = new Upgrader(
            this.player,
            this.horizontalProjectileLauncher,
            this.haloProjectileLauncher,
            this.diagonalProjectileLauncher,
            settings.upgrades
        );
        // 弹窗管理器
        const modalLauncher = new GameModalLauncher(AppRoot.Instance.ModalWindowManager, this.player, this.gamePauser, upgrader, translationData);
        // 物品管理器
        this.itemManager.init(this.enemyManager, this.player, this.gameResult, modalLauncher, settings.items);
        // 游戏 UI
        this.gameUI.init(this.player, modalLauncher, this.itemManager, this.gameResult);
        // 背景
        this.background.init(this.player.node);

        if (testValues) {
            this.timeAlive += testValues.startTime;
            this.player.Level.addXp(testValues.startXP);
        }
        // 音频管理器
        this.gameAudioAdapter.init(
            this.player,
            this.enemyManager,
            this.itemManager,
            this.horizontalProjectileLauncher,
            this.diagonalProjectileLauncher,
            this.haloProjectileLauncher
        );
    }

    private createPlayerData(settings: PlayerSettings, metaUpgrades: MetaUpgrades): PlayerData {
        const playerData: PlayerData = Object.assign(new PlayerData(), settings);
        // 最大生命值
        playerData.maxHp = metaUpgrades.getUpgradeValue(MetaUpgradeType.Health) + settings.defaultHP;
        // 每个等级所需经验值列表
        playerData.requiredXP = settings.requiredXP;
        // 速度
        playerData.speed = metaUpgrades.getUpgradeValue(MetaUpgradeType.MovementSpeed) + settings.speed;
        // 生命回复速度，每秒恢复多少生命值
        playerData.regenerationDelay = settings.regenerationDelay;
        // 经验采集
        playerData.xpMultiplier = metaUpgrades.getUpgradeValue(MetaUpgradeType.XPGatherer) + 1;
        // 金币采集
        playerData.goldMultiplier = metaUpgrades.getUpgradeValue(MetaUpgradeType.GoldGatherer) + 1;
        // 攻击伤害
        playerData.damage = metaUpgrades.getUpgradeValue(MetaUpgradeType.OverallDamage) + settings.weapon.damage;
        // 攻击间隔
        playerData.strikeDelay = settings.weapon.strikeDelay;
        // 磁力持续时间
        playerData.magnetDuration = settings.magnetDuration;

        return playerData;
    }
}

export class GameResult {
    public hasExitManually = false;
    public goldCoins = 0;
    public score = 0;
}
